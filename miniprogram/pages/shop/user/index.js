//获取应用实例
const app = getApp();
const util = require("../../../utils/util.js");
const db = wx.cloud.database()
Page({
  data: {
    Loading: 0,
    user_info: {},
    openid:'',
    isAdmin:false,
    color: app.ext.color,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },
  onLoad:function () {
    // // 获取用户信息
    // wx.getSetting({
    //   success: res => {
    //     if (res.authSetting['scope.userInfo']) {
    //       // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
    //       wx.getUserInfo({
    //         success: res => {
    //           this.setData({
    //             avatarUrl: res.userInfo.avatarUrl,
    //             userInfo: res.userInfo
    //           })
    //         }
    //       })
    //     } else {
    //       // TODO  跳转登录
    //       wx.navigateTo({
    //         url: '/pages/login/index'
    //       })
    //       // wx.authorize({
    //       //   scope: 'scope.userInfo',
    //       //   success: res => {
    //       //     this.setData({
    //       //       avatarUrl: res.userInfo.avatarUrl,
    //       //       userInfo: res.userInfo
    //       //     })
    //       //   }
    //       // })
    //     }
    //   }
    // })
    // this.onGetOpenid()

    if (wx.getStorageInfoSync('userid') == app.globalData.adminOpenID) {
      this.setData({
        isAdmin: true
      })
    } else {
      this.setData({
        isAdmin: true
      })
    }
  },
  onShow: function () {
    console.log(wx.getStorageSync('userInfo'))
    if (wx.getStorageSync('userInfo') == null || wx.getStorageSync('userInfo') == '' || wx.getStorageSync('openid') == null || wx.getStorageSync('openid') == ''){
      wx.navigateTo({
        url: '/pages/login/index'
      })
    }else{
      this.setData({
        logged: true,
        avatarUrl: wx.getStorageSync('userInfo').avatarUrl,
        userInfo: wx.getStorageSync('userInfo')
      })
    }
    // app.getUserInfo(user_info => {
    //   this.setData({
    //     userInfo: user_info
    //   });
    // });
  },
  onGetUserInfo: function (e) {
    if (!this.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
  },
  lookOverAll() {
    wx.navigateTo({
      url: '/pages/shop/order/order-list/index'
    })
  },
  goOrder(e) {
    wx.navigateTo({
      url: `/pages/shop/order/order-list/index?type=${e.currentTarget.dataset.type}`
    })
  },
  goApp(e) {
    // type  0：优惠券 1：管理中心 2：收货地址
    const type = e.currentTarget.dataset.type;
    if (type == 0) {
      wx.navigateTo({
        url: '/pages/shop/coupons/index'
      })
    }
    if (type == 1) {
      wx.navigateTo({
        url: '/pages/manage/nav/index'
      })
    }
    if (type == 2) {
      wx.chooseAddress({
        success: function (res) {
          console.log(res);
          console.log(res.userName);
          console.log(res.postalCode);
          console.log(res.provinceName);
          console.log(res.cityName);
          console.log(res.countyName);
          console.log(res.detailInfo);
          console.log(res.nationalCode);
          console.log(res.telNumber);
        }
      });
    }

    if (type == 3) {
      wx.navigateTo({
        url: '/pages/dy/index/index?isEdit=true'
      })
    }
  },
  //下拉刷新
  onPullDownRefresh: function () {
    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 600)
  },
  onGetUserInfo: function (e) {
    if (!this.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
  },
  onGetOpenid: function () {
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        this.setData({
          openid: res.result.openid
        })
        wx.setStorageSync("userid", res.result.openid)
        
        if (res.result.openid ==app.globalData.adminOpenID){
          this.setData({
            isAdmin:true
          })
        }else{
          this.setData({
            isAdmin:false
          })
        }
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
        // wx.navigateTo({
        //   url: '../deployFunctions/deployFunctions',
        // })
      }
    })
  },
  goback: function(e) {
    app.globalData.diyId = '';
    app.globalData.diyEdit = '';
    app.globalData.diyProduct = '';
    wx.redirectTo({
      url: '../../../pages/manage/xcx-man/index',
    })
  },
  goedit: function(e) {
    let pro = 0;
    db.collection('index_diy').where({
      _openid: wx.getStorageSync("openid") // 填入当前用户 openid
    }).count().then(res => {
      console.log(res.total)
      if (res.total > 2) {
        wx.showToast({
          icon: 'none',
          title: '最多免费拥有2个模板哦'
        });
      } else {
        db.collection('index_diy').doc(app.globalData.diyId).get().then(res => {
          console.log(res)
          if (res.data.all.length > 0) { //判断是否是首次diy编辑
            pro = res.data.product;
            db.collection('index_diy').add({
                // data 字段表示需新增的 JSON 数据
                data: {
                  type: "user",
                  createtime: util.formatTime(new Date()),
                  all: res.data.all,
                  name: res.data.name,
                  product: res.data.product,
                  files: []
                }
              })
              .then(res => {
                console.log(res);
                wx.setStorageSync('diyId', res._id);
                app.globalData.diyId = res._id;
                app.globalData.diyProduct = pro;
                app.globalData.diyPageI = -1;
                app.globalData.diyEdit = "true";
                wx.navigateTo({
                  url: `/pages/com/home/index`
                });
              })


          } else { //初始化些数据
            wx.showToast({
              title: '当前模板不可用',
            });
          }
        })
      }
    })
  },
  gobuy: function(e) {
    wx.makePhoneCall({
      phoneNumber: '17643036681' // 
    })
  }
});