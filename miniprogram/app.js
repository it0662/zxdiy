//app.js
const util = require("./utils/util.js");

App({

  ext: {
    color: "#3D94FF"
  },
  globalData: {
    tmpCurrentPageName: '',
    diyId:'',
    diyEdit:'',
    diyProduct:'',
    diyAll:[],
    diyName:'',
    diyPageI:-1
  },
  
  onLaunch: function () {
    
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
        //env: 'dz-2cb54c'
         env:'product-6d4b5e'
      })
    }
    if (wx.getStorageSync('userInfo') == null || wx.getStorageSync('userInfo') == '' || wx.getStorageSync('openid') == null || wx.getStorageSync('openid') == '') {
      wx.navigateTo({
        url: '/pages/login/index'
      })
      return;
    }else{
      if (wx.getStorageSync('openid') == 'ox7IZ4yeo5XWBLtMqgUUKkVMs_ks') {
        wx.setStorageSync("isAdmin", true);
      } else {
        wx.setStorageSync("isAdmin", false);
      }
      wx.cloud.callFunction({
        // 云函数名称
        name: 'user-add',
        // 传给云函数的参数
        data: {
          nick_name: wx.getStorageSync('userInfo').nickName,
          gender: wx.getStorageSync('userInfo').gender,
          language: wx.getStorageSync('userInfo').language,
          city: wx.getStorageSync('userInfo').city,
          province: wx.getStorageSync('userInfo').province,
          avatar_url: wx.getStorageSync('userInfo').avatarUrl,
          country: wx.getStorageSync('userInfo').country,
          createtime: util.formatTime(new Date()),
          openid: wx.getStorageSync('openid'),
          dz: -1
        },
        complete: res => {
          console.log('callFunction test result: ', res)
        }
      })
    }
  }
})
