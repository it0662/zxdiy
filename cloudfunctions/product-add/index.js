// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env:'product-6d4b5e'
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  try {
    if(event.id==''){
      return await db.collection('shop_product').add({
        // data 字段表示需新增的 JSON 数据
        data: {
          diyId:event.diyId,
          detail: event.proDetail,
          img_url: event.proImgUrl,
          name: event.proName,
          price: event.proNowPrice,
          price_org: event.proPrimePrice,
          subname: event.proDescription,
          sale: event.proSale,
          classify: event.proClassify,
          gg: event.proGg,
          yf: event.proYf,
          px: event.proPx,
          state: event.proState,
          unit:event.proUnit,
          createtime: event.createtime
        }
      })
    }else{
      return await db.collection('shop_product').doc(event.id).update({
        // data 字段表示需新增的 JSON 数据
        data: {
          detail: event.proDetail,
          img_url: event.proImgUrl,
          name: event.proName,
          price: event.proNowPrice,
          price_org: event.proPrimePrice,
          subname: event.proDescription,
          sale: event.proSale,
          classify: event.proClassify,
          unit:event.proUnit,
          gg: event.proGg,
          yf: event.proYf,
          px: event.proPx,
          state: event.proState,
          createtime: event.createtime
        }
      })
    }
    
  } catch (e) {
    console.error(e)
  }
}