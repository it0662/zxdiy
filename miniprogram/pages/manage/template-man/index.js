const db = wx.cloud.database()
const dataPageNum = 6
var app = getApp();
Page({
  data: {
    rankList: ["全部", "零售", "轻站"],
    orderType: 0, // 0："desc"降序   1："asc"升序
    orderBy: 'createtime', 
    selectIndex: 0,
    search_text: '',
    product_list: [],
    is_all: 0,
    page: 1,
    dataNum: 0,
    isSearch: 0,
    loadCount: 0,
    showCount: 0,
    isAdmin:false
  },
  onLoad: function (options) {
    this.setData({
      loadCount: this.data.loadCount + 1
    })
    if (options.text) {
      this.setData({
        search_text: options.text
      })
    }
    if (wx.getStorageSync("isAdmin")){
      this.setData({
        isAdmin:true
      })
    }
  },
  onShow: function () {
    this.setData({
      showCount: this.data.showCount + 1
    })
    if (this.data.showCount == this.data.loadCount) {
      this._clear();
      this.setData({
        dataNum: 0
      })
      this.getProducts(0);
    }
    var pages = getCurrentPages();
    var currPage = pages[pages.length-1];
    if (currPage.data.req==1){
      this._clear();
      this.setData({
        dataNum: 0
      })
      this.getProducts(0);
      this.setData({
        req: 0
      })
    }
  },
  _clear() {
    this.setData({
      page: 1,
      product_list: [],
      is_all: 0,
      orderType: 0,
      orderBy: 'createtime'
    })
  },
  bindconfirm(e) {
    this.data.search_text = e.detail;
    this._clear();
    this.getProducts(0);
  },
  getProducts(e) {
    let product_list = this.data.product_list;
    if ("" == this.data.search_text) {
      if (this.data.isSearch == 1)
        this.setData({
          dataNum: 0
        })
      if (e == 3) {
        this.setData({
          dataNum: 0
        })
      }
      if (this.data.selectIndex == 0) {   //查询零售和微站全部
        db.collection('index_diy').where({ type: 'template' }).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
          console.log(res.data);
          this.data.page++;
          const DATA = res.data;
          product_list = product_list.concat(DATA);
          this.setData({
            dataNum: this.data.dataNum + dataPageNum
          })
          if (product_list.length != this.data.dataNum) this.data.is_all = 1;
          this.setData({
            page: this.data.page,
            is_all: this.data.is_all,
            product_list: product_list,
            orderType: this.data.orderType
          });
        })
      } else {
        console.log("sdfsdf")
        console.log(this.data.selectIndex)
        db.collection('index_diy').where({type: 'template', product: this.data.selectIndex + "" }).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
          console.log(res.data);
          this.data.page++;
          const DATA = res.data;
          product_list = product_list.concat(DATA);
          this.setData({
            dataNum: this.data.dataNum + dataPageNum
          })
          if (product_list.length != this.data.dataNum) this.data.is_all = 1;
          this.setData({
            page: this.data.page,
            is_all: this.data.is_all,
            product_list: product_list,
            orderType: this.data.orderType
          });
        })
      }

      this.setData({
        isSearch: 0
      })
    } else {
      if (this.data.isSearch == 0) {
        this.setData({
          dataNum: 0
        })
      } else {
        if (e != 2) {
          this.setData({
            dataNum: 0
          })
        }
      }
      if (e == 3) {
        this.setData({
          dataNum: 0
        })
      }
      if (this.data.selectIndex == 0) {
        db.collection('index_diy').where({ type: 'template' }).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
          console.log(res);
          const DATA = res.data;
          let mData = [];
          var reg = new RegExp(this.data.search_text);
          for (var i = 0; i < DATA.length; i++) {
            if (DATA[i].name.match(reg)) {
              mData.push(DATA[i])
            }
          }

          this.data.page++;

          product_list = product_list.concat(mData);
          this.setData({
            dataNum: this.data.dataNum + dataPageNum
          })
          if (product_list.length != this.data.dataNum) this.data.is_all = 1;
          this.setData({
            page: this.data.page,
            is_all: this.data.is_all,
            product_list: product_list,
            orderType: this.data.orderType
          });
        })
      } else {
        db.collection('index_diy').where({ type: 'template', product: this.data.selectIndex + "" }).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
          console.log(res);
          const DATA = res.data;
          let mData = [];
          var reg = new RegExp(this.data.search_text);
          for (var i = 0; i < DATA.length; i++) {
            if (DATA[i].name.match(reg)) {
              mData.push(DATA[i])
            }
          }

          this.data.page++;

          product_list = product_list.concat(mData);
          this.setData({
            dataNum: this.data.dataNum + dataPageNum
          })
          if (product_list.length != this.data.dataNum) this.data.is_all = 1;
          this.setData({
            page: this.data.page,
            is_all: this.data.is_all,
            product_list: product_list,
            orderType: this.data.orderType
          });
        })
      }

      this.setData({
        isSearch: 1
      })
    }

  },
  onPullDownRefresh: function () {
    setTimeout(() => {
      this._clear();
      this.getProducts(1);
      wx.stopPullDownRefresh();
    }, 500)
  },
  onReachBottom: function () {
    if (!this.data.is_all) {
      this.getProducts(2);
    }
  },
  onAddProduct: function (e) {
    wx.navigateTo({
      url: `/pages/manage/template-add/index`
    });
  },
  onDelPro(e){
    const that = this;
    db.collection('index_diy').doc(e.currentTarget.dataset.id).remove({
      success(res){
       if(res.errMsg== "document.remove:ok"){
        that._clear();
          that.setData({
            dataNum: 0
          })
          that.getProducts(0);
           wx.showToast({
            title: '数据已被删除',
          });
         }else{
          wx.showToast({
            title: '数据删除失败',
          });
         }
          
      },
      fail: console.error
    })
  },
  onUptPro(e){
    wx.navigateTo({
      url: `/pages/manage/template-add/index?id=` + e.currentTarget.dataset.id
    });
  },
  onDiyPro(e){
    wx.setStorageSync('diyId', e.currentTarget.dataset.id);
    app.globalData.diyId = e.currentTarget.dataset.id;
    app.globalData.diyProduct = e.currentTarget.dataset.product;
    app.globalData.diyPageI = -1;
    app.globalData.diyEdit = "true";
    wx.navigateTo({
      url: `/pages/com/home/index?id=` + e.currentTarget.dataset.id
    });
  },
  onClickMan(e) {
    wx.setStorageSync('diyId', e.currentTarget.dataset.id);
    app.globalData.diyId = e.currentTarget.dataset.id;
    app.globalData.diyProduct = e.currentTarget.dataset.product;
    wx.navigateTo({
      url: `/pages/manage/nav/index`
    });
  },
  changeRank(e) {
    const index = Number(e.currentTarget.dataset.index);
    const type = Number(e.currentTarget.dataset.order_type);
    this.setData({
      page: 1,
      product_list: [],
      is_all: 0,
      selectIndex: index
    })
    this.getProducts(3);
  },
  onClickMainView(e) {
    console.log(123);
    wx.setStorageSync('diyId', e.currentTarget.dataset.id)

    db.collection('index_diy').where({
      _id: e.currentTarget.dataset.id
    }).get({
      success(res) {
        app.globalData.diyId = e.currentTarget.dataset.id;
          app.globalData.diyProduct = e.currentTarget.dataset.product;
          app.globalData.diyPageI = -1;
          app.globalData.diyEdit = "false";
          if (e.currentTarget.dataset.product == 1) {
            wx.switchTab({
              url: `/pages/com/index/index`
            });
          } else {
            wx.navigateTo({
              url: `/pages/com/home/index`
            });
          }
        // if ((res.data[0].all.length == 1 && res.data[0].product == "2") || (res.data[0].all.length == 0 && res.data[0].product == "1")) {
        //   wx.showToast({
        //     icon: "none",
        //     title: '正在努力设计中',
        //     duration: 2000
        //   })
        // } else {
          

        // }
      }
    })
  },
})