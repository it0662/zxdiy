# ZXDIY小程序开源版

#### 介绍
国内首个完全开源——基于小程序可视化DIY操作生成微信小程序，包括零售小程序、轻站小程序。基于小程序云开发实现，无需单独部署服务器，无论是个人还是公司，该款小程序非你莫属。

#### 软件架构
软件架构说明:微信小程序+云开发 


#### 安装教程

1. 通过微信开发者工具导入源码
2. 点击云开发，新建数据环境名称product，提交云开发代码
3. 然后新建数据集合，名称包括：coupon_info、index_diy、order_ana、order_info、pictures、shop_product、user_coupon、user_info、user_list
4. 编译、预览即可体验效果

#### DIY使用说明

1. 支持自由面板、文本、图片、菜单、搜索、轮播、商品、通告栏组件
2. 小程序中直接体验DIY操作，点击预览即可体验效果

#### 问题反馈

在使用中有任何问题，请使用以下联系方式联系我们

QQ群: 564126273(交流群①)

![](/pleaseDelete/s11.png)


#### 效果图
> 图一
<img src="/pleaseDelete/s1.png" width = "50%" height = "50%" div align=left/>

> 图二
<img src="/pleaseDelete/s2.png" width = "50%" height = "50%" div align=left/>

> 图三
<img src="/pleaseDelete/s3.png" width = "50%" height = "50%" div align=left/>

> 图四
<img src="/pleaseDelete/s4.png" width = "50%" height = "50%" div align=left/>

> 图五
<img src="/pleaseDelete/s5.png" width = "50%" height = "50%" div align=left/>

> 图六
<img src="/pleaseDelete/s6.png" width = "50%" height = "50%" div align=left/>

> 图七
<img src="/pleaseDelete/s7.png" width = "50%" height = "50%" div align=left/>

> 图八
<img src="/pleaseDelete/s8.png" width = "50%" height = "50%" div align=left/>

> 图九
<img src="/pleaseDelete/s9.png" width = "50%" height = "50%" div align=left/>

> 图十
<img src="/pleaseDelete/s10.png" width = "50%" height = "50%" div align=left/>